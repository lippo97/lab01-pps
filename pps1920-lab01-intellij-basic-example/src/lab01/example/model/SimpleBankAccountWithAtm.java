package lab01.example.model;

public class SimpleBankAccountWithAtm implements BankAccountWithAtm {

    private final BankAccount bankAccount;
    private final FeeCalculator depositFeeCalculator;
    private final FeeCalculator withdrawFeeCalculator;


    public SimpleBankAccountWithAtm(final AccountHolder holder, final double balance, final FeeCalculator depositFeeCalculator,
                                    final FeeCalculator withdrawFeeCalculator) {
        this.bankAccount = new SimpleBankAccount(holder, balance);
        this.depositFeeCalculator = depositFeeCalculator;
        this.withdrawFeeCalculator = withdrawFeeCalculator;
    }

    /**
     * Allows to know who is the holder of this bank account
     *
     * @return the AccountHolder instance related to this bank account.
     */
    @Override
    public AccountHolder getHolder() {
        return bankAccount.getHolder();
    }

    /**
     * Returns the current balance of the bank account
     *
     * @return the current balance
     */
    @Override
    public double getBalance() {
        return bankAccount.getBalance();
    }

    /**
     * Allows the deposit of an amount on the account, if the given usrID corresponds to the register holder ID
     * of the bank account. This ID acts like an "identification token" .
     *
     * @param usrID  the id of the user that wants do the deposit
     * @param amount the amount of the deposit
     */
    @Override
    public void depositWithAtm(int usrID, double amount) {
        this.bankAccount.deposit(usrID, this.depositFeeCalculator.apply(amount));
    }

    /**
     * Allows the withdraw of an amount from the account, if the given usrID corresponds to the register holder ID
     * of the bank account. This ID acts like an "identification token" .
     *
     * @param usrID  the id of the user that wants do the withdraw
     * @param amount the amount of the withdraw
     */
    @Override
    public void withdrawWithAtm(int usrID, double amount) {
        this.bankAccount.withdraw(usrID, this.withdrawFeeCalculator.apply(amount));
    }

    /**
     * Allows the deposit of an amount on the account, if the given usrID corresponds to the register holder ID
     * of the bank account. This ID acts like an "identification token" .
     *
     * @param usrID  the id of the user that wants do the deposit
     * @param amount the amount of the deposit
     */
    @Override
    public void deposit(int usrID, double amount) {
        this.bankAccount.deposit(usrID, amount);
    }

    /**
     * Allows the withdraw of an amount from the account, if the given usrID corresponds to the register holder ID
     * of the bank account. This ID acts like an "identification token" .
     *
     * @param usrID  the id of the user that wants do the withdraw
     * @param amount the amount of the withdraw
     */
    @Override
    public void withdraw(int usrID, double amount) {
        this.bankAccount.withdraw(usrID, amount);
    }
}
