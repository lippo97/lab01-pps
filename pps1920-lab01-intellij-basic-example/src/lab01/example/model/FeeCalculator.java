package lab01.example.model;

@FunctionalInterface
public interface FeeCalculator {

    /**
     * Applies a fee on a given amount of money.
     * @param amount the amount of money to tax
     * @return the amount of money with the fee applied
     */
    double apply(double amount);
}
